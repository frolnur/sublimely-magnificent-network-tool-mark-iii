#! /usr/bin/env python3

import PySimpleGUI as sg 
import os
import os.path
import sys
from scapy.all import *
from functions import *

# Window Theme
sg.theme('DarkBlue')

# First Window layout in 2 columns 

file_list_column = [
    [
        sg.Text("Select pcap Folder:"),
    ],
    [
        sg.In(size=(20,1), enable_events=True, key="-FOLDER-"),
        sg.FolderBrowse(),
    ],
    [
        sg.Listbox(
            values=[], enable_events=True, size=(40,5), key="-FILE LIST-"
        )
    ],
    [
        sg.Text('Count:'), sg.InputText(size=(20,1), enable_events=True, key="-COUNT-")
    ],
    [
        sg.Text('Interface:'), sg.InputText(size=(20,1), enable_events=True, key="-IFACE-")
    ],
    [
        sg.Text('Timeout'), sg.In(size=(20,1), enable_events=True, key="-TIMER-")
    ],
    [
        sg.Text('Network (CDIR notation)'), sg.In(size=(20,1), enable_events=True, key="-NET-")
    ],
    [
        sg.Button('ARP Scan'), sg.Button('DHCP Scan'),
        sg.Button('Active Ports'),
    ],
    [
        sg.Button('DNS Traffic'), sg.Button('HTTP Traffic'),
    ],
    [
        sg.Button('Network Scan'),
    ],
    [
        sg.Button('Clear'),
        sg.Button('Exit')
    ],
]

# Display the output of Scapy command
output_column = [
    [sg.Text("Output Display:")],
    [sg.Text(size=(40,1), key="-TOUT-")],
    [sg.Output(size=(90,40), key="-Outputs-")],
]

# ------- Full layout ----------
layout = [
    [
        sg.Column(file_list_column),
        sg.VSeperator(),
        sg.Column(output_column),

    ]
]

window = sg.Window("Network Analyizer/Scanner", layout)

# Run the loop
while True:
    event, values = window.read(timeout=400)
    # Setting variable for Count and Iface
    counts = values["-COUNT-"]
    ifaces = values["-IFACE-"]
    times = values["-TIMER-"]
    nets = values["-NET-"]

    # Exit or window closed, bread loop and close window
    if event == "Exit" or event == sg.WIN_CLOSED:
        break
    # Folder name was filled in. Make list of files
    if event == "-FOLDER-":
        folder = values["-FOLDER-"]
        try:
            # Get list of files from folder
            file_list = os.listdir(folder)
        except:
            file_list = []

        fnames = [
            f 
            for f in file_list
            if os.path.isfile(os.path.join(folder, f))
            and f.lower().endswith((".pcap"))
        ]
        window["-FILE LIST-"].update(fnames)

    elif event == "-FILE LIST-": # A file was chosen
        try:
            filename = os.path.join(
                values["-FOLDER-"], values["-FILE LIST-"][0]
            )
            pcap = rdpcap(filename)

        except:
            pass
    elif event == "ARP Scan":
        ## ARP Scan function goes here.
        try:
            if ifaces:
                if counts:
                    arp = sniff(iface=ifaces, prn=arp_scan, filter='arp', store=0, count=int(counts))
                    window["-TOUT-"].update(arp)
                    window["-Outputs-"].update(arp=arp)
                elif times:
                    arp = sniff(iface=ifaces, prn=arp_scan, filter='arp', store=0, timeout=int(times))
                    window["-TOUT-"].update(arp)
                    window["-Outputs-"].update(arp=arp)
                else:
                    print("Count or Timeout not specified")

            elif pcap:
                arp = sniff(offline=pcap, prn=arp_scan, filter = 'arp', store=0)
                window["-TOUT-"].update(arp)
                window["-Outputs-"].update(arp=arp)

            else: 
                print("Interface or pcap not specified.")

        except:
            pass
    elif event == "DHCP Scan":
        ## DHCP Scan goes here.
        try:
            if ifaces:
                if counts:
                    dhcp = sniff(iface=ifaces, filter='udp and (port 67 and 68)', prn=handle_dhcp_packet, count=int(counts))
                    window["-TOUT-"].update(dhcp)
                    window["-Outputs-"].update(dhcp=dhcp)
                elif times:
                    dhcp = sniff(iface=ifaces, filter='udp and (port 67 and 68)', prn=handle_dhcp_packet, timeout=int(times))
                    window["-TOUT-"].update(dhcp)
                    window["-Outputs-"].update(dhcp=dhcp)
                else:
                    print("Count or Timeout not specified")

            elif pcap:
                dhcp = sniff(offline=pcap, filter="udp and (port 67 and 68)", prn=handle_dhcp_packet)
                window["-TOUT-"].update(dhcp)
                window["-Outputs-"].update(dhcp=dhcp)
            else:
                print("Interface or pcap not specified.")

        except:
            pass
        
    elif event == "Active Ports":
        ## Port scan.
            try:
                if ifaces:
                    if counts:
                        ports = sniff(iface=ifaces, filter = 'ip', prn=port_scan, count=int(counts))
                        window["-TOUT-"].update(ports)
                        window["-Outputs-"].update(ports=ports)
                    elif times:
                        ports = sniff(iface=ifaces, filter = 'ip', prn=port_scan, timeout=int(times))
                        window["-TOUT-"].update(ports)
                        window["-Outputs-"].update(ports=ports)
                    else:
                        print("Count or Timeout not specified")
                elif pcap:
                    ports = sniff(offline=pcap, filter='ip', prn=port_scan)
                    window["-TOUT-"].update(ports)
                    window["-Outputs-"].update(ports=ports)
                else: 
                    print("Interface or pcap not specified.")

            except:
                pass
    elif event == "HTTP Traffic":
        ## again more code here
        try:
            if ifaces:
                if counts:
                    httpTraff = sniff(iface=ifaces, prn=http_sniff, lfilter= lambda p: "GET" in str(p), filter= "tcp and (port 80 or 443)", count=int(counts))
                    window["-TOUT-"].update(httpTraff)
                    window["-Outputs-"].update(httpTraff=httpTraff)
                elif times:
                    httpTraff = sniff(iface=ifaces, prn=http_sniff, lfilter= lambda p: "GET" in str(p), filter= "tcp and (port 80 or 443)", timeout=int(times))
                    window["-TOUT-"].update(httpTraff)
                    window["-Outputs-"].update(httpTraff=httpTraff)
                else:
                    print("Counter or Timeout not specified")
            elif pcap:
                httpTraff = sniff(offline=pcap, prn=http_sniff, lfilter= lambda p: "GET" in str(p), filter= "tcp and (port 80 or 443)")
                window["-TOUT-"].update(httpTraff)
                window["-Outputs-"].update(httpTraff=httpTraff)
            else: 
                print("Interface or pcap not specified.")

        except:
            pass
    elif event == "DNS Traffic":
        try:
            if ifaces:
                if counts:
                    dnsTraff = sniff(iface=ifaces, filter = "port 53", prn=dns_query, store=0, count=int(counts))
                    window["-TOUT-"].update(dnsTraff)
                    window["-Outputs-"].update(dnsTraff=dnsTraff)
                elif times:
                    dnsTraff = sniff(iface=ifaces, filter = "port 53", prn=dns_query, store=0, timeout=int(times))
                    window["-TOUT-"].update(dnsTraff)
                    window["-Outputs-"].update(dnsTraff=dnsTraff)
                else:
                    print("Counter or Timeout not specified.")
            elif pcap:
                dnsTraff = sniff(offline=pcap, filter = "port 53", prn=dns_query, store=0)
                window["-TOUT-"].update(dnsTraff)
                window["-Outputs-"].update(dnsTraff=dnsTraff)
            else:
                print("Interface and pcap not specified.")

        except:
            pass

    elif event == "Clear":
        try:
            window["-TOUT-"].update('')
            window["-Outputs-"].update('')  
        except:
            pass
    elif event == 'Network Scan':
        try:
            if ifaces:
                netscan = sniff(iface=ifaces, prn=net_scan, count=1)
                window["-TOUT-"].update(netscan)
                window["-Outputs-"].update(netscan=netscan)
            if pcap:
                netscan = sniff(offline=pcap, prn=net_scan, count=1)
                window["-TOUT-"].update(netscan)
                window["-Outputs-"].update(netscan=netscan)
            else:
                print("Interface or pcap not Specified.")

        except:
            pass

window.close()

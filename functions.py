#! /usr/bin/env python3

from scapy.all import *
import pprint 
import sys
import random


stars = lambda n: "*" * n

### This file contians all of the necessary functions for the application... 
###
###### This is the function that will scan for open ports. 
#
def port_scan(pkt):
    if IP in pkt:
        ip_src=pkt[IP].src
        ip_dst=pkt[IP].dst
    if TCP in pkt:
        d_port=pkt[TCP].dport
        print("IP Source" + " "*6 + "Port" + " "*6 + "IP Dest")
        print(str(ip_src) +" "*2+ str(d_port)+ " "*6 + str(ip_dst))


#### This function will sniff for HTTP GET traffic and display results. 
#
def http_sniff(pkt):

    return "\n".join((
        stars(30) + "GET PACKET" + stars(30),
        "\n".join(pkt.sprintf("{Raw:%Raw.load%}").split(r"\r\n")),
        stars(70)))


### This is the function for the ARP scan
#
def arp_scan(pkt):
    if ARP in pkt and pkt[ARP].op in (1,2):
        return pkt.sprintf("%ARP.psrc% --> %ARP.hwsrc%")

# function to extract dhcp_options by key
### Is used for the DHCP scan
#
def get_option(dhcp_options, key):

    must_decode = ['hostname', 'domain', 'vender_class_id']
    try: 
        for i in dhcp_options:
            if i[0] == key:
                 # if dhcp server returned multiple name servers
                 # return all as comman seperated string.
                if key == 'name_server' and len(i) > 2:
                    return ",".join(i[1:])
                     #domain and hostname are binary strings,
                     # decode to unicode string before returning
                elif key in must_decode:
                    return i[1].decode()
                else:
                    return i[1]

    except:
        pass 

### This the main part for the DHCP scan
##### function above is used within this function
#
def handle_dhcp_packet(pkt):

    # match dhcp discover

    if DHCP in pkt and pkt[DHCP].options[0][1] == 2:
        print('---')
        print('New DHCP Offer')

        subnet_mask = get_option(pkt[DHCP].options, 'subnet_mask')
        lease_time = get_option(pkt[DHCP].options, 'lease_time')
        router = get_option(pkt[DHCP].options, 'router')
        print(f"DHCP Server {pkt[IP].src} ({pkt[Ether].src}) offered {pkt[BOOTP].yiaddr}")
        print(f"DHCP Options: subnet_mask: {subnet_mask}, lease_time: {lease_time}, router: {router}")

    # Match DCHP request
    elif DHCP in pkt and pkt[DHCP].options[0][1] == 3: 
        print('---')
        print('New DHCP Request')

        requested_addr = get_option(pkt[DHCP].options, 'requested_addr')
        hostname = get_option(pkt[DHCP].options, 'hostname')
        print(f"host {hostname} ({pkt[Ether].src}) requested {requested_addr}")

    # Match DHCP ack
    elif DHCP in pkt and pkt[DHCP].options[0][1] == 5:
        print('---')
        print('New DHCP Ack')

        subnet_mask = get.option(pkt[DHCP].options, 'subnet_mask')
        lease_time = get.option(pkt[DHCP].options, 'lease_time')
        router = get.option(pkt[DHCP].options, 'router')

        print(f"DHCP Sever {pkt[IP].src} ({pkt[Ether].src}) "
            f"acked {pkt[BOOTP].yiaddr}")
        
        print(f"DHCP Options: subnet_mask: {subnet_mask}, lease_time: "
            f"{lease_time}, router: {router}")

    # Match DHCP inform
    elif DHCP in pkt and pkt[DHCP].options[0][1] == 8:
        print('---')
        print('New DHCP Inform')

        hostname = get_option(pkt[DHCP].options, 'hostname')
        vendor_class_id = get_option(pkt[DHCP].options, 'vendor_class_id')

        print(f"DHCP Inform from {pkt[IP].src} ({pkt[Ether].src}) "
             f"hostname: {hostname}, vendor_class_id: {vendor_class_id}")

    else:
        print('---')
        print('Some Other DHCP Packet')
        print(pkt.summary())
        print(ls(pkt))

    return

##### This is the function for the DNS scan
#
def dns_query(pkt):
     if IP in pkt:
         ip_src = pkt[IP].src
         ip_dst = pkt[IP].dst
         if pkt.haslayer(DNS) and pkt.getlayer(DNS).qr == 0:
             print(str(ip_src) + " -> " + str(ip_dst) + " : " + "(" + str(pkt.getlayer(DNS).qd.qname) + ")")

def net_scan(nets):
    #nets = '192.168.1.1/24'

    arp = ARP(pdst=nets)
    
    ether = Ether(dst="ff:ff:ff:ff:ff:ff")

    packet = ether/arp

    result = srp(packet, timeout=3, verbose=0)[0]

    # list of clients
    clients = []

    for sent, received in result:
        clients.append({'ip': received.psrc, 'mac': received.hwsrc})

    # print results
    print('Availalbe devices in the network:')

    print("IP" + " "*18+"MAC")

    for client in clients:
      print("{:16}    {}".format(client['ip'], client['mac']))

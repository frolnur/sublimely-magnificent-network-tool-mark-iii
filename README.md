# Sublimely Magnificent Network Tool Mark III

This is a Python tool and utilizes the Scapy library, that can be used to preform basic scans of a network. This tool uses the sniff function within Scapy and can either be used on a .pcap file or over the wire on an interface of the users choosing. 

This tool can scan for the following traffic
- DNS query scans
- Active Ports
- HTTP
- DHCP 

## Installation

This tool requires Python along with the Scapy library

Use [pip](https://pip.pypa.io/en/stable/) to install Scapy.

The recomend way to install Scapy is:
```bash
pip install --pre scapy[basic]
```
You can find more information about different ways to install Scapy [here](https://scapy.readthedocs.io/en/latest/installation.html)

## Usage 

for Scapy to work correctly root/admin or some other elevated permissions are needed. 

The application can be launched as such:

```bash
sudo ./smntmiii.py
```

A few works in progress to note. 
- A pcap file or and Interface must be selected/applied
- If both a pcap and Interface are supplied the program will run off the Interface so make sure only one is filled in
- Specifing count will tell the program how many packets to sniff

*** PLEASE NOTE that since having a count of '0' is considered infinity in scapy scanning the wire will not work. There needs to be an actual number of packets to scan otherwise the program will run indefinitely. *** 
- There are some known issues with DHCP and HTTP GET scans not always working correctly.
- These scans can either run off of packet counts or a timeout when running off the wire, it should be noted that if using packet counts or really long timeout times the program may seem unresponsive. 
- When scanning a pcap file the count and timeout values are not used. The scan will sniff the whole file
*** Network Scan function was added late and is not intended to work at this time ***

# Roadmap

- Fix issues with DHCP and HTTP GET scans
- Work out search filed in output section, making sifting through data easier. 
- Change ARP scan from passively scanning the network waiting for packets to a more aggressive apporach
- Rewrite GUI from PysimpleGUI Tkinter to QT version

# License 

[GPLv3](https://choosealicense.com/licenses/gpl-3.0/)